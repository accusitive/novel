#![feature(proc_macro_span)]
extern crate lexpr;
extern crate proc_macro;
extern crate proc_macro2;
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
#[allow(unused_omport)]
use proc_macro_hack::proc_macro_hack;
use quote::quote;

use serde::{Deserialize, Serialize};
use serde_lexpr::{from_str, to_string};

#[derive(Serialize, Deserialize, Debug)]
struct El(String, String); // Name Body

#[proc_macro]
pub fn parse_element(ts: TokenStream) -> TokenStream {
    let token2stream = TokenStream2::from(ts);
    let tokenstring = token2stream.to_string();
    // let expr: syn::Expr = syn::parse_macro_input!(ts as syn::Expr);

    println!("parse element got otkenstring {}", tokenstring);
    let (el_name, el_body): (String, String) =
        from_str(&tokenstring).unwrap_or(("Failed".into(), "Parsing".into()));
    // let _h_name = h.0;
    // let h_body = h.1;

    if el_name == "h" {
        return TokenStream::from(quote! {
            Header::new(#el_body.into())
        });
    } else {
        return TokenStream::from(quote! {
            Paragraph::new(#el_body.into())
        });
    }
}
