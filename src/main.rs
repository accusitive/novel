use novelmacros::parse_element as pe;
#[derive(Debug)]
struct Header{
    body: String
}
#[derive(Debug)]
struct Paragraph{
    body: String
}
#[derive(Debug)]
struct StringElement{
    text: String
}

trait Element {
    fn new(body: String) -> Self;
    fn render(&self) -> String{
        return "".into();
    }
}


impl Element for Header{
    fn new(body: String) -> Header{
        return Header {
            body: body
        };
    }
    fn render(&self) -> String{
        return String::from(format!("<h>{}</h>", self.body));
    }
}
impl Element for Paragraph{
    fn new(body: String) -> Paragraph{
        return Paragraph {
            body: body
        };
    }
    fn render(&self) -> String{
        return String::from(format!("<p>{}</p>", self.body));
    }
}
impl Element for StringElement{
    fn new(text: String) -> StringElement{
        return StringElement {
            text: text
        };
    }
    fn render(&self) -> String{
        return String::from(format!("{}", self.text));
    }
}
fn main() {

    println!("{:?}", pe!{x});
    
    
    // let h = Header::new("Header world".into());
    // println!("{:?}", h.render());
    // let h2 = pe!{("h" "Paragraph defined via pe")};
    
    
    // println!("{:?}", h2.render())
}
